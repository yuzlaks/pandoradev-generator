<?php

use App\Http\Controllers\Finance\IncomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;

Route::prefix('finance/administrator')->group(function () {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('products', ProductController::class);
    Route::resource('transaction', TransactionController::class);
    
    // 
    Route::resource('income', IncomeController::class);
});