<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $data = [
            [
                'name' => 'Laptop',
                'detail' => 'RAM 4GB CORE I3',
                'price' => 3000000
            ],
            [
                'name' => 'HP',
                'detail' => 'RAM 4GB',
                'price' => 2000000
            ],
            [
                'name' => 'Iphone 12',
                'detail' => 'RAM 4GB',
                'price' => 12000000
            ]
        ];
        Product::insert($data);
    }
}
