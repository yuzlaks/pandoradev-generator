<?php
  
namespace Database\Seeders;
  
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Database\Factories\UserFactory;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
  
class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = User::create(
        [
            'name' => 'Administrator', 
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456')
        ]);
      
        $role = Role::create(['name' => 'Admin']);
        
        $permissions = Permission::pluck('id','id')->all();
        
        $role->syncPermissions($permissions);
        
        $user->assignRole([$role->id]);

        
        $roleGuest = Role::create(['name' => 'Guest']);

        $permissions = Permission::where('id', 9)->pluck('name')->first();
        
        $roleGuest->syncPermissions($permissions);
        
        $user->assignRole([$roleGuest->id]);

        //syncRole
        User::create([
            'name' => 'Guest', 
            'email' => 'guest@gmail.com',
            'password' => bcrypt('123456')
        ]);

        DB::table('model_has_roles')->insert([
            ['role_id' => 2, 'model_type' => 'App\Models\User', 'model_id' => 2],
        ]);
        
    }
}