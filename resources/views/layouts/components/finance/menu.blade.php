<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <li class="nav-header">Master</li>
    @can('role-list')
        <li class="nav-item">
            <a href="{{ route('roles.index') }}" class="nav-link @yield('menu-role')">
                <i class="nav-icon fas fa-shield-alt"></i>
                <p>
                    Role Management
                </p>
            </a>
        </li>
    @endcan
    @can('user-list')
        <li class="nav-item">
            <a href="{{ route('users.index') }}" class="nav-link @yield('menu-user')">
                <i class="nav-icon fas fa-user-cog"></i>
                <p>
                    Users Management
                </p>
            </a>
        </li>
    @endcan

    <li class="nav-header">Finance</li>
    @can('income-list')
        <li class="nav-item">
            <a href="{{ route('income.index') }}" class="nav-link @yield('menu-income')">
                <i class="nav-icon fas fa-suitcase"></i>
                <p>
                    Income
                </p>
            </a>
        </li>
    @endcan
</ul>