<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
</head>

<body style="background-color: #f1f2f6">
    <div class="container mt-4">
        <center>
            <h1><b>Hi</b></h1>
        </center>
        <div class="col-md-6 offset-md-3 mt-5">
            <div class="card card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="100px">URL</th>
                            <th width="200px">Deskripsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><code>/login</code></td>
                            <td>Untuk masuk ke halaman login</td>
                        </tr>
                        <tr>
                            <td><code>/register</code></td>
                            <td>Untuk masuk ke halaman register</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="alert alert-info mt-4">
                Project ini dilengkapi dengan <b>sistem role</b> (management role)
                yang mana anda bisa mengatur dengan leluasa di dalamnya
            </div>
        </div>
    </div>
</body>

</html>
