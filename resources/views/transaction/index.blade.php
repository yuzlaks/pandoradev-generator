@extends('layouts.app')
@section('menu-transaction','active')
@section('page-name','Data Transaction')
@section('breadcrumb')
  <li class="breadcrumb-item"><a href="#">Home</a></li>
  <li class="breadcrumb-item active">DataTables</li>
@endsection()

@section('content')
    <div class="col-12">
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Data Transactions</h2>
                <div class="pull-right" style="float:right">
                    <a class="btn btn-success" href="{{ route('transaction.create') }}"> Create New Transaction</a>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Code</th>
                            <th>Date</th>
                            <th>User</th>
                            <th>Grand Total</th>
                            <th width="280px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $key => $item)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $item->transaction_code }}</td>
                                <td>{{ $item->transaction_date }}</td>
                                <td>{{ @$item->user->name }}</td>
                                <td>{{ $item->grand_total }}</td>
                                <td>
                                    <a class="btn btn-info" href="{{ route('transaction.show', $item->id) }}">Show</a>
                                    <a class="btn btn-primary" href="{{ route('transaction.edit', $item->id) }}">Edit</a>
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['transaction.destroy', $item->id], 'style' => 'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->

    {!! $data->render() !!}


    <p class="text-center text-primary"><small> </small></p>
@endsection
