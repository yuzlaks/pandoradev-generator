@extends('layouts.app')
@section('menu-transaction', 'active')
@section('page-name', 'Transaction')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
    <li class="breadcrumb-item active">Transaction</li>
@endsection()

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Create Transaction</h2>
                <div class="pull-right" style="float:right">
                    <a class="btn btn-primary" href="{{ route('transaction.index') }}"> Back</a>
                </div>
            </div>
            <div class="card-body">
                {!! Form::open(['route' => 'transaction.store', 'method' => 'POST']) !!}
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Name:</strong>
                            {!! Form::text('', Auth::user()->name, ['placeholder' => 'Name', 'class' => 'form-control', 'disabled']) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Date:</strong>
                            {!! Form::date('transaction_date', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Description:</strong>
                            <textarea name="description" class="form-control" id="" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2">
                        <div class="form-group">
                            <strong>Grand Total:</strong>
                            <input type="text" name="" readonly class="form-control display-grand-total">
                            <input type="text" style="display:none" name="grand_total" class="form-control grand-total">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-left mb-3">
                        <button type="button" class="btn btn-dark add-detail"><i class="fa fa-plus"></i></button>
                    </div>
                    <table class="table table-bordered">
                        <thead class="thead-dark">
                            <tr>
                                <th>Product</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Sub Total</th>
                                <th width="10px">Action</th>
                            </tr>
                        </thead>
                        <tbody class="transaction-detail"></tbody>
                    </table>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-right">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $('.add-detail').click(function(){
            var option = '';

            $.ajax({
                url : "{{ url('get_product') }}",
                type : "GET",
                dataType : "JSON",
                success:function(res){
                    $.each(res, function(k, v){
                        option += `<option data-price="${v.price}" value="${v.id}">${v.name}</option>`;
                    });
                    var data  = `<tr>`;
                            data += `<td>
                                        <select name="product_id[]" class="form-control select-product">
                                            <option disabled selected>- Choose Product -</option>
                                            ${option}
                                        </select>
                                     </td>`;
                            data += `<td><input disabled="true" name="qty[]" type="number" class="form-control qty"></td>`;
                            data += `<td><input name="price[]" type="number" readonly class="form-control price"></td>`;
                            data += `<td><input name="sub_total[]" type="number" readonly class="form-control sub-total"></td>`;
                            data += `<td>
                                        <button class="btn btn-danger delete"><i class="fa fa-trash"></i></button>
                                     </td>`;
                        data += `</tr>`;
        
                    $('.transaction-detail').append(data);
                }
            });
        });

        $('body').on('click','.delete', function(){
            $(this).closest('tr').remove();

            grand_total();

        })

        $('body').on('change','.select-product', function(){
            var price = $(this).find(":selected").data('price');

            $(this).closest('tr').find('.price').val(price);
            $(this).closest('tr').find('.qty').removeAttr('disabled');

            // grand_total();

        })

        $('body').on('keyup','.qty', function(){
            
            var qty   = $(this).val();
            var price = $(this).closest('tr').find('.price').val();

            $(this).closest('tr').find('.sub-total').val(price * qty);

            grand_total();

        })

        function grand_total(newgrandtotal = 0){
            $('table > tbody  > tr').each(function(index, val) {
                newgrandtotal += tonumeric($(val).find('td').eq(3).find('input[type=number]').val(),'rp');
            });

            console.log(newgrandtotal);

            $('.display-grand-total').val(rupiah(newgrandtotal));
            $('.grand-total').val(newgrandtotal);

        }

    </script>
@endpush