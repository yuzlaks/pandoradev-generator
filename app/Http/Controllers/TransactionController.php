<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:transaction-list|transaction-create|transaction-edit|transaction-delete', ['only' => ['index','store']]);
         $this->middleware('permission:transaction-create', ['only' => ['create','store']]);
         $this->middleware('permission:transaction-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:transaction-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Transaction::with('user')->orderBy('id','DESC')->paginate(5);

        return view('transaction.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transaction.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transaction                   = new Transaction();
        $transaction->user_id          = Auth::user()->id;
        $transaction->transaction_code = 'TRANS'.rand(15,9999);
        $transaction->transaction_date = $request->transaction_date;
        $transaction->description      = $request->description;
        $transaction->grand_total      = $request->grand_total;
        $transaction->status           = 0;
        $transaction->save();
    
        foreach ($request->product_id as $key => $value) {

            TransactionDetail::create([
                "transaction_id" => $transaction->id,
                "product_id"     => $request['product_id'][$key],
                "qty"            => $request['qty'][$key],
                "price"          => $request['price'][$key],
                "sub_total"      => $request['sub_total'][$key],
            ]);

        }

        return redirect('transaction');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction::with('transaction_detail')->find($id);
        $product     = Product::get();
        return view('transaction.show', compact('transaction','product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaction = Transaction::with('transaction_detail')->find($id);
        $product     = Product::get();
        return view('transaction.edit', compact('transaction','product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Transaction::where('id', $id)->update([
            "transaction_date" => $request->transaction_date,
            "description"      => $request->description,
            "grand_total"      => $request->grand_total,
            "status"           => 0
        ]);

        TransactionDetail::where('transaction_id', $id)->delete();

        foreach ($request->product_id as $key => $value) {

            TransactionDetail::create([
                "transaction_id" => $id,
                "product_id"     => $request['product_id'][$key],
                "qty"            => $request['qty'][$key],
                "price"          => $request['price'][$key],
                "sub_total"      => $request['sub_total'][$key],
            ]);

        }

        return redirect('transaction');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Transaction::where('id', $id)->delete();
        TransactionDetail::where('transaction_id', $id)->delete();

        return redirect('transaction');
    }
}
