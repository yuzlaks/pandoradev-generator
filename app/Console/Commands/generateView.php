<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class generateView extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pandora:view {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create trinity view';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('name');

        Storage::disk('view')->put($name.'/data.blade.php', '');
        Storage::disk('view')->put($name.'/form.blade.php', '');
        Storage::disk('view')->put($name.'/form-edit.blade.php', '');

        $msg = "<fg=green;>* Success create trinity view</>
><fg=yellow> resources/views/$name/data.blade.php </>
><fg=yellow> resources/views/$name/form.blade.php </>
><fg=yellow> resources/views/$name/form-edit.blade.php </>";

        $this->line($msg);
    }
}
