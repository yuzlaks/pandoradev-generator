<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Permission;

class generateController extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pandora:controller {name} {--super}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make some controller with pandora function';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        
        $originalName = $this->argument('name');
        
        // untuk keperluan edit-edit
        $name = $this->argument('name');
        
        $namespace = $name;

        if (str_contains($name, '/')) { 
            $explodename = (explode('/',$name));
            $name = end($explodename);
            $namespace = array_pop($explodename);
            $namespace = implode("",$explodename);
        }
        
        $modifyName = str_replace('Controller', '', $name);
        $modifyName = strtolower($modifyName);

        if ($this->option('super')) {
            $this->contentSuper($namespace, $name, $originalName, $modifyName);
        }else{
            $this->content($namespace, $name, $originalName, $modifyName);
        }
    }

    public function content($namespace, $name, $originalName, $modifyName)
    {
        $content = "<?php

namespace App\Http\Controllers\\$namespace;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class $name extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        '$'this->middleware('permission:$modifyName-list|$modifyName-create|$modifyName-edit|$modifyName-delete', ['only' => ['index','show']]);
        '$'this->middleware('permission:$modifyName-create', ['only' => ['create','store']]);
        '$'this->middleware('permission:$modifyName-edit', ['only' => ['edit','update']]);
        '$'this->middleware('permission:$modifyName-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('".strtolower(str_replace('Controller','',$originalName))."/data');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('".strtolower(str_replace('Controller','',$originalName))."/form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  '$'request
     * @return \Illuminate\Http\Response
     */
    public function store(Request '$'request)
    {
        return redirect('".strtolower(str_replace('Controller','',$name))."');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  '$'id
     * @return \Illuminate\Http\Response
     */
    public function show('$'id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  '$'id
     * @return \Illuminate\Http\Response
     */
    public function edit('$'id)
    {
        return view('".strtolower(str_replace('Controller','',$originalName))."/form-edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  '$'request
     * @param  int  '$'id
     * @return \Illuminate\Http\Response
     */
    public function update(Request '$'request, '$'id)
    {
        return redirect('".strtolower(str_replace('Controller','',$name))."');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  '$'id
     * @return \Illuminate\Http\Response
     */
    public function destroy('$'id)
    {
        return redirect('".strtolower(str_replace('Controller','',$name))."');
    }
}";
            
        $content = str_replace("'$'", '$', $content);

        $msg = "<fg=green;>* Resource controller</>
<fg=green;>* Generate role system</>
<fg=green;>Success create controller => <fg=yellow>app/Http/Controllers/$originalName.php </>";

        $this->line($msg);

        Storage::disk('controller')->put($originalName.'.php', $content);
    }

    public function contentSuper($namespace, $name, $originalName, $modifyName)
    {
        $content = "<?php

namespace App\Http\Controllers\\$namespace;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\\$namespace\\".str_replace('Controller','',$name).";

class $name extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        '$'this->middleware('permission:$modifyName-list|$modifyName-create|$modifyName-edit|$modifyName-delete', ['only' => ['index','show']]);
        '$'this->middleware('permission:$modifyName-create', ['only' => ['create','store']]);
        '$'this->middleware('permission:$modifyName-edit', ['only' => ['edit','update']]);
        '$'this->middleware('permission:$modifyName-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('".strtolower(str_replace('Controller','',$originalName))."/data');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('".strtolower(str_replace('Controller','',$originalName))."/form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  '$'request
     * @return \Illuminate\Http\Response
     */
    public function store(Request '$'request)
    {
        return redirect('".strtolower(str_replace('Controller','',$name))."');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  '$'id
     * @return \Illuminate\Http\Response
     */
    public function show('$'id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  '$'id
     * @return \Illuminate\Http\Response
     */
    public function edit('$'id)
    {
        return view('".strtolower(str_replace('Controller','',$originalName))."/form-edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  '$'request
     * @param  int  '$'id
     * @return \Illuminate\Http\Response
     */
    public function update(Request '$'request, '$'id)
    {
        return redirect('".strtolower(str_replace('Controller','',$name))."');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  '$'id
     * @return \Illuminate\Http\Response
     */
    public function destroy('$'id)
    {
        return redirect('".strtolower(str_replace('Controller','',$name))."');
    }
}";
            
        $content = str_replace("'$'", '$', $content);

        $nameFile = strtolower(str_replace('Controller','',$originalName));

        $msg = "+-----------------------------------------------
|       Pandoradev Super Console (PSC)
+-----------------------------------------------
<fg=green;>• Resource controller</>
<fg=green;>• Create & connect with model $nameFile</>
<fg=green;>• Generate role system</>
<fg=green;>• Create migration for $nameFile</>

<fg=black;bg=green>Create multiple files</>
<fg=green;>* Success create controller
><fg=yellow> app/Http/Controllers/$originalName.php </>

<fg=green;>* Success create model
><fg=yellow> app/Models/".str_replace('Controller','',$originalName).".php </>

<fg=green;>* Success create trinity view</>
><fg=yellow> resources/views/$nameFile/data.blade.php </>
><fg=yellow> resources/views/$nameFile/form.blade.php </>
><fg=yellow> resources/views/$nameFile/form-edit.blade.php </>

Copyright (c) The Pandoradev Technologies";

        $this->line($msg);


        Storage::disk('view')->put($nameFile.'/data.blade.php', '');
        Storage::disk('view')->put($nameFile.'/form.blade.php', '');
        Storage::disk('view')->put($nameFile.'/form-edit.blade.php', '');

        Storage::disk('controller')->put($originalName.'.php', $content);

        $this->createModel($originalName);
        $this->createPermission(strtolower(str_replace('Controller','',$name)));
    }

    public function createModel($originalName)
    {
        $originalName = str_replace('Controller','',$originalName);
        return exec("php artisan make:model $originalName -m");
    }

    public function createPermission($name)
    {
        $permissions = [
            "$name-list",
            "$name-create",
            "$name-edit",
            "$name-delete",
         ];
       
         foreach ($permissions as $permission) {
              Permission::create(['name' => $permission]);
         }
    }
}
