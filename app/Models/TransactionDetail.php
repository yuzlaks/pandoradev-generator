<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    use HasFactory;

    protected $table = 'transaction_details';

    protected $primaryKey = 'id';

    protected $fillable = [
        'transaction_id',
        'product_id',
        'qty',
        'price',
        'sub_total'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
